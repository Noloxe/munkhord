import { Client, Collection, Intents } from 'discord.js';
import fs from 'fs';
import mongooseConnect from './utils/mongoose';
import initApp from './utils/initApp';
import auth from './utils/auth';

import dotenv from 'dotenv';
import logError from './utils/tools/admin/logError';
dotenv.config();

const token = process.env.TOKEN;

const client = new Client({ intents: [Intents.FLAGS.GUILDS, Intents.FLAGS.GUILD_MESSAGES] });
client.mongoose = mongooseConnect;
client.commands = new Collection();
client.selectCommands = new Collection();
client.buttonCommands = new Collection();

fs.readdir('src/commands/', (err, dirs) => {
	if (err) return console.error;

	dirs.forEach((dir) => {
		fs.readdir(`src/commands/${dir}`, (errFiles, files) => {
			if (errFiles) return console.error;

			files.forEach(async (file) => {
				if (!file.endsWith('.js')) return undefined;
				const command = await import(`./commands/${dir}/${file}`);
				return client.commands.set(command.default.data.name, command.default);
			});
		});
	});
});

fs.readdir('src/selectcommands/', (err, dirs) => {
	if (err) return console.error;

	dirs.forEach((dir) => {
		fs.readdir(`src/selectcommands/${dir}`, (errFile, files) => {
			if (errFile) return console.error;

			files.forEach(async (file) => {
				if (!file.endsWith('.js')) return undefined;
				const command = await import(`./selectcommands/${dir}/${file}`);
				return client.selectCommands.set(command.default.name, command.default);
			});
		});
	});
});

fs.readdir('src/buttoncommands/', (err, dirs) => {
	if (err) return console.error;

	dirs.forEach((dir) => {
		fs.readdir(`src/buttoncommands/${dir}`, (errFile, files) => {
			if (errFile) return console.error;

			files.forEach(async (file) => {
				if (!file.endsWith('.js')) return undefined;
				const command = await import(`./buttoncommands/${dir}/${file}`);
				return client.buttonCommands.set(command.default.name, command.default);
			});
		});
	});
});

fs.readdir('src/events/', (err, files) => {
	if (err) return console.error;

	files.forEach(async (file) => {
		if (!file.endsWith('.js')) return undefined;

		const evt = await import(`./events/${file}`);
		const evtName = file.split('.')[0];

		return client.on(evtName, evt.default.bind(null, client));
	});
});

client.on('interactionCreate', async (interaction) => {
	if (!interaction.isCommand()) return;

	const command = client.commands.get(interaction.commandName);

	if (!command) return;

	const hasAuthError = auth(command.auth, { channel: interaction.channel.name, playerRoles: interaction.member.roles.cache });

	if (hasAuthError) return interaction.reply({ content: hasAuthError.message, ephemeral: true });

	try {
		await command.execute(interaction);
	} catch (error) {
		console.error(error);
		logError(interaction, error);
	}
});

client.on('interactionCreate', async (interaction) => {
	if (!interaction.isSelectMenu()) return;
	
	const command = client.selectCommands.get(interaction.customId);

	if (!command) return;

	try {
		await command.execute(interaction);
	} catch (error) {
		console.error(error);
		logError(interaction, error);
	}
});

client.on('interactionCreate', async (interaction) => {
	if (!interaction.isButton()) return;
	const [commandName, targetId, value] = interaction.customId.split('_');
	const command = client.buttonCommands.get(commandName);

	if (!command) return;

	const hasAuthError = auth(command.auth, { channel: interaction.channel.name, playerRoles: interaction.member.roles.cache });

	if (hasAuthError) return interaction.reply({ content: hasAuthError.message, ephemeral: true });

	try {
		await command.execute(interaction, targetId, value);
	} catch (error) {
		console.error(error);
		logError(interaction, error);
	}
});

client.mongoose.init();
client.login(token);
client.on('ready', () => initApp(client));
client.on('error', console.error);
