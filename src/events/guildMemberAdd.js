export const evt = (client, member) => {
	const channel = client.channels.find((r) => r.name === 'general');
	channel.send(`${member} veut partir à l'aventure`);
};

export default evt;
