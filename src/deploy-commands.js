import fs from 'fs';
// eslint-disable-next-line import/no-extraneous-dependencies
import { REST } from '@discordjs/rest';
// eslint-disable-next-line import/no-extraneous-dependencies
import { Routes } from 'discord-api-types/v9';
import dotenv from 'dotenv';

dotenv.config();

const { CLIENT_ID, GUILD_ID, TOKEN } = process.env;

const initCommands = async () => {
	console.log('init')
	const commandDirs = fs.readdirSync('src/commands');
	const commands = [];
	await Promise.all(commandDirs.map(async (dir) => {
		const commandFiles = fs.readdirSync(`src/commands/${dir}`).filter(file => file.endsWith('.js'));
		return Promise.all(commandFiles.map(async (file) => {
			const command = await import(`./commands/${dir}/${file}`);
			return commands.push(command.default.data.toJSON());
		}))
	}));
	return commands;
}

const rest = new REST({ version: '9' }).setToken(TOKEN);

(async () => {
	try {
		const commands = await initCommands();
		await rest.put(
			Routes.applicationGuildCommands(CLIENT_ID, GUILD_ID),
			{ body: commands },
		);

		console.log('Successfully registered application commands.');
	} catch (error) {
		console.error(error);
	}
})();
