const addPlayerRole = async (interaction, roleName, playerId) => {
	const role = interaction.member.guild.roles.cache.find((r) => r.name === roleName);
	const player = interaction.member.guild.members.cache.find((p) => p.id === playerId);
	return player.roles.add(role);
};

export default addPlayerRole;
