import { MessageEmbed } from 'discord.js';
import { CHANNELS_NAME, EMOJIS_ID } from '../../constants/global.js';

const logError = async (interaction, error) => {
	const targetErrorChannel = interaction.guild.channels.cache.find((c) => c.name === CHANNELS_NAME.LOG_ERRORS);
	console.log('Erreur dans LOGERROR.js', error);
	const communityName = interaction.guild.channels.cache.find((c) => c.id === interaction.channel.parentId);
	const errorMessage = new MessageEmbed()
		.addFields(
			{ name: 'Joueur', value: interaction.user.username },
			{ name: 'Communauté', value: communityName ? communityName.name : '-' },
			{ name: 'Salon', value: `<#${interaction.channel.id}>` },
			{ name: 'Message', value: error.toString() },
		);
	await targetErrorChannel.send({ embeds: [errorMessage] });
	// eslint-disable-next-line max-len
	return interaction.reply(`${EMOJIS_ID.ERROR} Erreur lors de l'éxécution de la commande. Merci de contacter un administrateur. ${EMOJIS_ID.ERROR}`, { ephemeral: true });
};

export default logError;
