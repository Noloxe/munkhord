import { MessageActionRow, MessageSelectMenu } from 'discord.js';

export default async (interaction, commandName, placeholder, list, content) => {
	const row = new MessageActionRow()
		.addComponents(
			new MessageSelectMenu()
				.setCustomId(`${commandName}`)
				.setPlaceholder(placeholder)
				.addOptions(list.map((item) => ({
					label: item.name,
					value: item._id,
				}))),
		);

	return interaction.reply({ content, components: [row] });
};
