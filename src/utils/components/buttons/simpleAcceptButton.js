import { MessageActionRow, MessageButton } from 'discord.js';

export const SIMPLE_ACCEPT_BUTTON_VALUES = {
	ACCEPT: 'ACCEPT',
	REFUSED: 'REFUSED',
};

export const simpleAcceptButton = async (commandName, targetId, targetChannel, content) => {
	const row = new MessageActionRow()
		.addComponents(
			new MessageButton()
				.setCustomId(`${commandName}_${targetId}_${SIMPLE_ACCEPT_BUTTON_VALUES.ACCEPT}`)
				.setLabel('Accepter')
				.setStyle('PRIMARY'),
		)
		.addComponents(
			new MessageButton()
				.setCustomId(`${commandName}_${targetId}_${SIMPLE_ACCEPT_BUTTON_VALUES.REFUSED}`)
				.setLabel('Refuser')
				.setStyle('DANGER'),
		);

	await targetChannel.send({ content, components: [row] });
};
