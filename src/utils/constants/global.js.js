/* eslint-disable max-len */
export const PERMISSIONS = {
	SPECIFIC_CHANNEL: 'SPECIFIC_CHANNEL',
	SPECIFIC_ROLE: 'SPECIFIC_ROLE',
	FORBIDDEN_ROLES: 'FORBIDDEN_ROLES',
};

export const ROLES_NAME = {
	IN_GAME: 'En jeu',
	GAME_ADMIN: 'Admin Partie',
	ZONE: {
		COMMUNITY: 'Dans la Communauté',
		OUTSIDE_ONE: 'Dans la plaine',
		OUTSIDE_TWO: 'Dans la forêt',
		OUTSIDE_THREE: 'Dans la ville',
	},
};

export const CHANNELS_NAME = {
	GAME_CREATION: 'regroupement',
	LOG: 'log',
	LOG_ERRORS: 'log-errors',
	LAUNCH_GAME: {
		PUBLIC: 'création',
		PRIVATE: 'privé',
	},
	GAME: {
		GLOBAL: {
			name: 'radio',
			private: false,
			message: "Vous êtes ici dans la Zone Publique, qui vous permettra d'échanger avec les autres Communautés, et de les tenir informées de l'avancement de la vôtre, via les informations quotidiennes",
		},
		COMMUNITY: {
			name: 'la-communauté',
			private: true,
			role: ROLES_NAME.ZONE.COMMUNITY,
			message: "C'est un peu la place centrale de votre Communauté, l'endroit où vous pourrez échanger au sujet de votre organisation, de la répartition des tâches, etc... Pas de commandes disponibles dans ce salon, vous êtes seulement là pour discuter (ou vous engueuler). ",
		},
		WORKSHOP: {
			name: 'l-atelier',
			private: true,
			role: ROLES_NAME.ZONE.COMMUNITY,
			message: "Ce sera la zone à tout faire, l'endroit où vous taperez vos commandes pour tout ce qui concerne la communauté (construire des batiments, etc....).",
		},
		INFORMATION_PANEL: {
			name: 'panneau-d-informations',
			private: true,
			role: ROLES_NAME.ZONE.COMMUNITY,
			message: 'Ici apparaitront les messages informatifs et mis en page concernant votre communauté. Vous y verrez notamment le rapport journalier des attaques, ou des réponses à certaines de vos commandes (tel que la liste des batiments présents, etc...).',
		},
		OUTSIDE_ONE: {
			name: 'la-plaine',
			private: true,
			role: ROLES_NAME.ZONE.OUTSIDE_ONE,
			message: "La première zone extérieur: La plaine qui entoure le lieu où vous vous êtes posés. Ici, vous pourrez utiliser toutes les commandes qui ne s'utilisent pas en ville, afin de faire du stock (qu'il faudra penser à ramener en ville avant l'attaque, pour ne pas... enfin... voilà quoi...).",
		},
		OUTSIDE_TWO: {
			name: 'la-forêt',
			private: true,
			role: ROLES_NAME.ZONE.OUTSIDE_TWO,
			message: "Après avoir traversé la Plaine, vous arriverez en Forêt. Vous trouverez ici un peu plus de matériel et de nourriture qu'en Plaine, mais bon, y'a un peu plus de chemin pour rentrer...",
		},
		OUTSIDE_THREE: {
			name: 'la-ville',
			private: true,
			role: ROLES_NAME.ZONE.OUTSIDE_THREE,
			message: "Je pense que vous avez compris, ici, on est encore plus loin, mais y'a encore plus de chose! Ou alors, c'est juste que vous aurez vider les autres zones, donc pas le choix, faudra venir jusqu'ici. Beaucoup de matériel, par contre, qui sait, vous ne serez peut être pas seul... Alors tâchez de garder un peu de souffle pour rebrousser chemin...!",
		},
	},
};

export const EMOJIS_ID = {
	ERROR: ':exclamation:',
};
