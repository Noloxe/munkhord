export const RESOURCES_LIST = {
	TIME: {
		id: 'TIME',
		name: 'Temps',
		emoji: ':watch:',
	},
	WOOD: {
		id: 'WOOD',
		name: 'Bois',
		emoji: ':wood:',
	},
	METAL: {
		id: 'METAL',
		name: 'Métal',
		emoji: ':gear:',
	},
	VEGETABLES: {
		id: 'VEGETABLES',
		name: 'Fruit',
		emoji: ':apple:',
	},
	MEAT: {
		id: 'MEAT',
		name: 'Viande',
		emoji: ':poultry_leg:',
	},
	DEFENSE: {
		id: 'DEFENSE',
		name: 'Défense',
		emoji: ':shield:',
	},
	MEDICINE: {
		id: 'MEDICINE',
		name: 'Plante Médicinale',
		emoji: ':herb:',
	},
	WATER: {
		id: 'WATER',
		name: 'Eau',
		emoji: ':droplet:',
	},
};

export const initialCommunityResources = [
	{
		_id: RESOURCES_LIST.WOOD.id,
		quantity: 5,
		maxQuantity: 15,
		...RESOURCES_LIST.WOOD,
	},
	{
		_id: RESOURCES_LIST.METAL.id,
		quantity: 5,
		maxQuantity: 15,
		...RESOURCES_LIST.METAL,
	},
	{
		_id: RESOURCES_LIST.VEGETABLES.id,
		quantity: 5,
		maxQuantity: 5,
		...RESOURCES_LIST.VEGETABLES,
	},
	{
		_id: RESOURCES_LIST.MEAT.id,
		quantity: 0,
		maxQuantity: 10,
		...RESOURCES_LIST.MEAT,
	},
	{
		_id: RESOURCES_LIST.MEDICINE.id,
		quantity: 1,
		maxQuantity: 5,
		...RESOURCES_LIST.MEDICINE,
	},
	{
		_id: RESOURCES_LIST.WATER.id,
		quantity: 5,
		maxQuantity: 5,
		...RESOURCES_LIST.WATER,
	},
	{
		_id: RESOURCES_LIST.DEFENSE.id,
		quantity: 0,
		...RESOURCES_LIST.DEFENSE,
	},
];

export const initialPlayerResources = [
	{
		_id: RESOURCES_LIST.WOOD.id,
		quantity: 0,
		maxQuantity: 5,
		...RESOURCES_LIST.WOOD,
	},
	{
		_id: RESOURCES_LIST.METAL.id,
		quantity: 0,
		maxQuantity: 5,
		...RESOURCES_LIST.METAL,
	},
	{
		_id: RESOURCES_LIST.VEGETABLES.id,
		quantity: 0,
		maxQuantity: 5,
		...RESOURCES_LIST.VEGETABLES,
	},
	{
		_id: RESOURCES_LIST.MEAT.id,
		quantity: 0,
		maxQuantity: 10,
		...RESOURCES_LIST.WOOD,
	},
	{
		_id: RESOURCES_LIST.MEDICINE.id,
		quantity: 0,
		maxQuantity: 15,
		...RESOURCES_LIST.MEDICINE,
	},
	{
		_id: RESOURCES_LIST.WATER.id,
		quantity: 0,
		maxQuantity: 5,
		...RESOURCES_LIST.WATER,
	},
	{
		_id: RESOURCES_LIST.TIME.id,
		quantity: 10,
		...RESOURCES_LIST.TIME,
	},
];
