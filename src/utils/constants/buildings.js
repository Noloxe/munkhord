import { RESOURCES_LIST } from './resources';
import { BUILDINGS_ID } from '../../database/models/constants';

export default [
	{
		_id: BUILDINGS_ID.WALL,
		name: 'Mur',
		bonus: { resourceId: RESOURCES_LIST.DEFENSE.id, quantity: 2 },
		destructible: true,
		constructionResources: [
			{
				resourceId: RESOURCES_LIST.WOOD.id,
				quantity: 0,
				requirement: 5,
			},
			{
				resourceId: RESOURCES_LIST.METAL.id,
				quantity: 0,
				requirement: 5,
			},
			{
				resourceId: RESOURCES_LIST.TIME.id,
				quantity: 0,
				requirement: 12,
			},
		],
		level: { max: 10 },
		initial: true,
	},
	{
		_id: BUILDINGS_ID.TANKER,
		name: 'Citerne',
		bonus: { resourceId: RESOURCES_LIST.WATER.id, quantity: 6 },
		destructible: true,
		constructionResources: [
			{
				resourceId: RESOURCES_LIST.WOOD.id,
				quantity: 0,
				requirement: 2,
			},
			{
				resourceId: RESOURCES_LIST.METAL.id,
				quantity: 0,
				requirement: 3,
			},
			{
				resourceId: RESOURCES_LIST.TIME.id,
				quantity: 0,
				requirement: 10,
			},
		],
		level: { max: 5 },
		initial: true,
	},
	{
		_id: BUILDINGS_ID.WORKSHOP,
		name: 'Atelier',
		destructible: false,
		constructionResources: [
			{
				resourceId: RESOURCES_LIST.WOOD.id,
				quantity: 0,
				requirement: 5,
			},
			{
				resourceId: RESOURCES_LIST.METAL.id,
				quantity: 0,
				requirement: 5,
			},
			{
				resourceId: RESOURCES_LIST.TIME.id,
				quantity: 0,
				requirement: 10,
			},
		],
		level: { max: 5 },
		initial: true,
	},
	{
		_id: BUILDINGS_ID.FIELD,
		name: 'Champ',
		production: { resourceId: RESOURCES_LIST.VEGETABLES.id, quantity: 2 },
		destructible: true,
		prerequisite: BUILDINGS_ID.OFFICE,
		constructionResources: [
			{
				resourceId: RESOURCES_LIST.WOOD.id,
				quantity: 0,
				requirement: 2,
			},
			{
				resourceId: RESOURCES_LIST.METAL.id,
				quantity: 0,
				requirement: 2,
			},
			{
				resourceId: RESOURCES_LIST.TIME.id,
				quantity: 0,
				requirement: 15,
			},
		],
		level: { max: 5 },
	},
	{
		_id: BUILDINGS_ID.OFFICE,
		name: 'Bureau',
		destructible: false,
		constructionResources: [
			{
				resourceId: RESOURCES_LIST.WOOD.id,
				quantity: 0,
				requirement: 2,
			},
			{
				resourceId: RESOURCES_LIST.METAL.id,
				quantity: 0,
				requirement: 2,
			},
			{
				resourceId: RESOURCES_LIST.TIME.id,
				quantity: 0,
				requirement: 10,
			},
		],
	},
];
