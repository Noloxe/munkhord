import { PERMISSIONS } from './constants/global.js';

const auth = (authObjects, { channel, playerRoles }) => {
	if (!authObjects) return false;

	const isAuth = authObjects.map((authObject) => {
		switch (authObject.type) {
		case PERMISSIONS.SPECIFIC_CHANNEL:
			if (authObject.channel === channel) {
				return { result: true };
			}
			return { result: false, message: "Cette commande ne s'utilise pas dans ce salon" };
		case PERMISSIONS.FORBIDDEN_ROLES: {
			let response = { result: true };
			authObject.roles.forEach((forbiddenRole) => {
				if (playerRoles.find((r) => r.name === forbiddenRole)) {
					response = { result: false, message: authObject.error };
				}
			});
			return response;
		}
		case PERMISSIONS.SPECIFIC_ROLE: {
			let response = { result: false, message: authObject.error };
			authObject.roles.forEach((specificRole) => {
				if (playerRoles.find((r) => r.name === specificRole)) {
					response = { result: true };
				}
			});
			return response;
		}
		default:
			return { result: true };
		}
	});

	return isAuth.find((a) => !a.result);
};

export default auth;
