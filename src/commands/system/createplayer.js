import { SlashCommandBuilder } from '@discordjs/builders';
import { createPlayer } from '../../database/queries/player';
import logError from '../../utils/tools/admin/logError';

const command = {
	data: new SlashCommandBuilder()
		.setName('créer_joueur')
		.setDescription("Ajout d'un joueur en base de donnée")
		.addStringOption((option) => option
			.setName('id')
			.setDescription('ID de l\'utilisateur')
			.setRequired(true)),
	async execute(interaction) {
		try {
			const userID = interaction.options.getString('id');
			const user = await interaction.client.users.fetch(userID);
			const player = await createPlayer({ _id: user.id, ...user });
			return interaction.reply(`Le joueur ${player.username} a bien été ajouté en base de données`);
		} catch (e) {
			return logError(interaction, e);
		}
	},
	auth: [],
};

export default command;
