import { SlashCommandBuilder } from '@discordjs/builders';
import { deleteCommunity, findCommunity } from '../../database/queries/community';
import logError from '../../utils/tools/admin/logError';

const command = {
	data: new SlashCommandBuilder()
		.setName('outil_suppression_commu')
		.setDescription('Suppression d\'une communauté et toutes ses dépendances')
		.addStringOption((option) => option
			.setName('nom')
			.setDescription('Nom de la Communauté')
			.setRequired(true)),
	async execute(interaction) {
		const { options } = interaction;
		const name = options.getString('nom');

		try {
			const category = interaction.guild.channels.cache.find((c) => c.name === name);
			const channels = interaction.guild.channels.cache.filter((c) => c.parentId === category.id);
			const role = interaction.guild.roles.cache.find((r) => r.name === name);

			await Promise.all(channels.map(async (c) => c.delete()));
			await category.delete();
			await role.delete();

			const community = await findCommunity({ name });
			await deleteCommunity(community.id);

			return interaction.reply(`La Communauté "${name}" a bien été supprimée.`);
		} catch (e) {
			return logError(interaction, e);
		}
	},
};

export default command;
