import { SlashCommandBuilder } from '@discordjs/builders';
import { getCommunity } from '../../database/queries/community';
import simpleSelect from '../../utils/components/selects/simpleSelect';
import { CHANNELS_NAME, PERMISSIONS, ROLES_NAME } from '../../utils/constants/global.js';
import buildings from '../../utils/constants/buildings';
import { RESOURCES_LIST } from '../../utils/constants/resources';
import logError from '../../utils/tools/admin/logError';

export const BUILD_COMMAND_NAME = 'build';

const hasResources = (communityResources, buildingResources) => {
	const countResources = buildingResources.map((r) => {
		if (r.resourceId === RESOURCES_LIST.TIME.id) {
			return true;
		}
		const availableResource = communityResources.find((communityResource) => communityResource._id === r.resourceId);
		if (availableResource.quantity >= r.requirement) {
			return true;
		}
		return false;
	});
	return countResources.every((e) => e);
};

const hasPrerequisite = (
	communityBuildings,
	prerequisite,
) => {
	if (!prerequisite) {
		return true;
	}
	return !!communityBuildings.find((communityBuilding) => communityBuilding._id === prerequisite);
};

const command = {
	data: new SlashCommandBuilder()
		.setName('construire_batiment')
		.setDescription('Sélectionner un bâtiment et le Temps que vous aller y passer pour lancer sa construction'),
	async execute(interaction) {
		try {
			const community = await getCommunity(interaction.channel.parentId);
			const buildingsList = buildings.filter(
				(building) => !community.buildings.find((b) => b._id === building._id)
				&& hasResources(community.resources, building.constructionResources)
				&& hasPrerequisite(community.buildings, building.prerequisite),
			);
			if (!buildingsList.length) {
				return interaction.reply('Aucun batiment disponible à la construction');
			}
			return simpleSelect(
				interaction,
				BUILD_COMMAND_NAME,
				'Choisir bâtiment',
				buildingsList,
				'Quel bâtiment voulez-vous construire?',
			);
		} catch (e) {
			return logError(interaction, e);
		}
	},
	auth: [
		{
			type: PERMISSIONS.SPECIFIC_CHANNEL,
			channel: CHANNELS_NAME.GAME.WORKSHOP.name,
		},
		{
			type: PERMISSIONS.SPECIFIC_ROLE,
			roles: [ROLES_NAME.ZONE.COMMUNITY],
			error: "Vous n'êtes pas présent dans ce lieu",
		},
	],
};

export default command;
