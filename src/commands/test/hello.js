import { SlashCommandBuilder } from '@discordjs/builders';

const command = {
	data: new SlashCommandBuilder()
		.setName('test')
		.setDescription('Test commande')
		.addUserOption((option) => option.setName('target').setDescription('The user\'s avatar to show')),
	async execute(interaction) {
		const user = interaction.options.getUser('target');
		if (user) return interaction.reply(`${user.username}'s avatar: ${user.displayAvatarURL({ dynamic: true })}`);
		return interaction.reply(`Your avatar: ${interaction.user.displayAvatarURL({ dynamic: true })}. Bot déployé!`);
	},
};

export default command;
