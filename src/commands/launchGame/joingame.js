import { SlashCommandBuilder } from '@discordjs/builders';
import { CHANNELS_NAME, PERMISSIONS, ROLES_NAME } from '../../utils/constants/global.js';
import { simpleAcceptButton } from '../../utils/components/buttons/simpleAcceptButton';
import logError from '../../utils/tools/admin/logError.js';

export const JOINGAME_COMMAND_NAME = 'joingame';

const command = {
	data: new SlashCommandBuilder()
		.setName('rejoindre_communauté')
		.setDescription('Demander à rejoindre une Communauté en création'),
	async execute(interaction) {
		const { user, channel, member } = interaction;
		try {
			const categoryId = channel.parentId;
			const communityName = member.guild.channels.cache.find((c) => c.id === categoryId).name;
			const privateChannel = member.guild.channels.cache.find((c) => c.name === CHANNELS_NAME.LAUNCH_GAME.PRIVATE
			&& c.parentId === categoryId);
			const gameAdminRole = interaction.member.guild.roles.cache.find((r) => r.name === ROLES_NAME.GAME_ADMIN);
			const joinMessage = `${gameAdminRole} **${user.username}** demande à rejoindre la Communauté`;
			await simpleAcceptButton(JOINGAME_COMMAND_NAME, user.id, privateChannel, joinMessage);
			return interaction.reply(`Votre demande a été envoyé à la Communauté **${communityName}**`);
		} catch (e) {
			return logError(interaction, e);
		}
	},
	auth: [
		{
			type: PERMISSIONS.SPECIFIC_CHANNEL,
			channel: CHANNELS_NAME.LAUNCH_GAME.PUBLIC,
		},
		{
			type: PERMISSIONS.FORBIDDEN_ROLES,
			roles: [ROLES_NAME.IN_GAME],
			error: 'Vous participez déjà à une Communauté',
		},
	],
};

export default command;
