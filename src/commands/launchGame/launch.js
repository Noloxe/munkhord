import { Permissions } from 'discord.js';
import { SlashCommandBuilder } from '@discordjs/builders';
import { getCommunity, updateCommunity } from '../../database/queries/community';
import { CHANNELS_NAME, PERMISSIONS, ROLES_NAME } from '../../utils/constants/global.js';
import { COMMUNITY } from '../../database/models/constants';
import { updatePlayer } from '../../database/queries/player';
import { initialPlayerResources, initialCommunityResources } from '../../utils/constants/resources';
import buildings from '../../utils/constants/buildings';
import addPlayerRole from '../../utils/tools/addMemberRole';
import logError from '../../utils/tools/admin/logError';

const command = {
	data: new SlashCommandBuilder()
		.setName('installer_communauté')
		.setDescription('Lance une partie'),
	async execute(interaction) {
		const { channel, member } = interaction;
		try {
			const categoryId = channel.parentId;
			const community = await getCommunity(categoryId);

			const bots = member.guild.members.cache.filter((m) => m.user.bot);
			const communityRole = await member.guild.roles.cache.find((r) => r.name === community.name);
			const everyoneRole = member.guild.roles.everyone;

			const actualChannels = await member.guild.channels.cache.filter((c) => c.parentId === categoryId);

			await Promise.all(Object.values(CHANNELS_NAME.GAME).map(async (channelData) => {
				const newChannel = await member.guild.channels.create(channelData.name);
				await newChannel.setParent(categoryId);
				if (channelData.private) {
					await Promise.all(bots.map(
						async (bot) => newChannel.permissionOverwrites.create(bot.id, { [Permissions.FLAGS.VIEW_CHANNEL]: true }),
					));
					await newChannel.permissionOverwrites.create(communityRole.id, { [Permissions.FLAGS.VIEW_CHANNEL]: true });
					await newChannel.permissionOverwrites.create(everyoneRole.id, { [Permissions.FLAGS.VIEW_CHANNEL]: false });
				}
				return newChannel.send(channelData.message);
			}));

			const updatedCommunity = {
				...community,
				status: COMMUNITY.STATUS.RUNNING,
				resources: initialCommunityResources,
				buildings: buildings.filter((building) => building.initial),
			};

			await updateCommunity(community._id, updatedCommunity);

			await Promise.all(community.players.map(
				async (player) => {
					await updatePlayer(player._id, { ...player, resources: initialPlayerResources });
					return addPlayerRole(interaction, ROLES_NAME.ZONE.COMMUNITY, player._id);
				},
			));
			const deleteChannels = () => actualChannels.map((c) => c.delete());
			setTimeout(deleteChannels, 5000);
			// eslint-disable-next-line max-len
			return interaction.reply('La Communauté est installée. Les deux salons de paramétrage vont être supprimé dans quelques secondes.');
		} catch (e) {
			return logError(interaction, e);
		}
	},
	auth: [
		{
			type: PERMISSIONS.SPECIFIC_CHANNEL,
			channel: CHANNELS_NAME.LAUNCH_GAME.PUBLIC,
		},
		{
			type: PERMISSIONS.SPECIFIC_ROLE,
			roles: [ROLES_NAME.GAME_ADMIN],
			error: "Vous n'êtes pas l'administrateur de cette Communauté",
		},
		// TODO Permissions en fonction du nombre de joueurs minimum requis
	],
};

export default command;
