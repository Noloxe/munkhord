import { Permissions } from 'discord.js';
import { SlashCommandBuilder } from '@discordjs/builders';
import { createCommunity, findCommunity } from '../../database/queries/community';
import { CHANNELS_NAME, PERMISSIONS, ROLES_NAME } from '../../utils/constants/global.js';
import addPlayerRole from '../../utils/tools/addMemberRole';
import logError from '../../utils/tools/admin/logError';

const command = {
	data: new SlashCommandBuilder()
		.setName('créer_communauté')
		.setDescription('Création d\'une nouvelle Communauté')
		.addStringOption((option) => option
			.setName('nom')
			.setDescription('Création d\'une Communauté')
			.setRequired(true)),
	async execute(interaction) {
		const { options, user, member } = interaction;
		const name = options.getString('nom');

		const isExist = await findCommunity({ name });
		if (isExist) {
			return interaction.reply(`Le nom "${name}" est déjà utilisé`);
		}
		try {
			const bots = member.guild.members.cache.filter((m) => m.user.bot);

			const category = await member.guild.channels.create(name, { type: 'GUILD_CATEGORY' });

			const communityRole = await member.guild.roles.create({ name });
			const everyoneRole = member.guild.roles.everyone;

			await addPlayerRole(interaction, communityRole.name, member.id);
			await addPlayerRole(interaction, ROLES_NAME.IN_GAME, member.id);
			await addPlayerRole(interaction, ROLES_NAME.GAME_ADMIN, member.id);

			const launchChannel = await member.guild.channels.create(CHANNELS_NAME.LAUNCH_GAME.PUBLIC);
			const privateChannel = await member.guild.channels.create(CHANNELS_NAME.LAUNCH_GAME.PRIVATE);

			await launchChannel.setParent(category.id);
			await privateChannel.setParent(category.id);

			await Promise.all(bots.map(
				async (bot) => privateChannel.permissionOverwrites.create(bot.id, { [Permissions.FLAGS.VIEW_CHANNEL]: true }),
			));
			await privateChannel.permissionOverwrites.create(communityRole.id, { [Permissions.FLAGS.VIEW_CHANNEL]: true });
			await privateChannel.permissionOverwrites.create(everyoneRole.id, { [Permissions.FLAGS.VIEW_CHANNEL]: false });

			const community = await createCommunity({
				_id: category.id,
				name,
				players: [{ _id: user.id, ...user }],
				categoryId: category.id,
			});

			return interaction.reply(
				`La Communauté "${community.name}" a bien été créée. Vous pouvez la paramétrer dans ce salon: <#${launchChannel.id}>`,
			);
		} catch (e) {
			return logError(interaction, e);
		}
	},
	auth: [
		{
			type: PERMISSIONS.SPECIFIC_CHANNEL,
			channel: CHANNELS_NAME.GAME_CREATION,
		},
		{
			type: PERMISSIONS.FORBIDDEN_ROLES,
			roles: [ROLES_NAME.IN_GAME],
			error: 'Vous participez déjà à une Communauté',
		},
	],
};

export default command;
