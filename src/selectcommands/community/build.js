import { BUILD_COMMAND_NAME } from '../../commands/community/build';
import { addCommunityBuilding } from '../../database/queries/community';
import buildings from '../../utils/constants/buildings';
import logError from '../../utils/tools/admin/logError';

const command = {
	name: BUILD_COMMAND_NAME,
	async execute(interaction) {
		try {
			const building = buildings.find((b) => b._id === interaction.values[0]);
			await addCommunityBuilding(interaction.channel.parentId, building);
			// eslint-disable-next-line max-len
			interaction.reply('La bâtiment est en construction. Les membres de la Communauté peuvent maintenant participer à l\'avancement des travaux avec la commande **/participer**');
		} catch (e) {
			return logError(interaction, e);
		}
		return interaction.message.delete();
	},
};

export default command;
