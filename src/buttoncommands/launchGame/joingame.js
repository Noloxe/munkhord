import { JOINGAME_COMMAND_NAME } from '../../commands/launchGame/joingame';
import { getCommunity, updateCommunity } from '../../database/queries/community';
import { SIMPLE_ACCEPT_BUTTON_VALUES } from '../../utils/components/buttons/simpleAcceptButton';
import { PERMISSIONS, ROLES_NAME } from '../../utils/constants/global.js';
import addPlayerRole from '../../utils/tools/addMemberRole';
import logError from '../../utils/tools/admin/logError';

const command = {
	name: JOINGAME_COMMAND_NAME,
	async execute(interaction, targetId, value) {
		const askerPlayer = interaction.member.guild.members.cache.find((m) => m.id === targetId);
		try {
			if (value === SIMPLE_ACCEPT_BUTTON_VALUES.ACCEPT) {
				const communityId = interaction.channel.parentId;
				const community = await getCommunity(communityId);
				const newCommunityData = {
					players: [...community.players, { _id: askerPlayer.user.id, ...askerPlayer.user }],
					...community,
				};
				await updateCommunity(communityId, newCommunityData);
				addPlayerRole(interaction, community.name, targetId);
				addPlayerRole(interaction, ROLES_NAME.IN_GAME, targetId);
				await interaction.channel.send(`${askerPlayer} a été accepté dans la Communauté`);
			} else {
				await interaction.channel.send(`${askerPlayer} n'a pas été accepté dans la Communauté`);
			}
		} catch (e) {
			return logError(interaction, e);
		}
		return interaction.message.delete();
	},
	auth: [
		{
			type: PERMISSIONS.SPECIFIC_ROLE,
			roles: [ROLES_NAME.GAME_ADMIN],
			error: "Vous n'êtes pas l'administrateur de cette Communauté",
		},
	],
};

export default command;
