import Player from '../models/players';

export const createPlayer = async (data) => {
	const player = new Player(data);
	return player.save();
};

export const updatePlayer = async (id, data) => {
	const playerToUpdate = await Player.findById(id);
	if (!playerToUpdate) {
		throw new Error('Player not found');
	}
	playerToUpdate.set(data);
	return playerToUpdate.save();
};

export const getPlayer = async (id) => {
	const player = await Player.findById(id);
	if (!player) {
		return null;
	}
	return player;
};

export const getPlayers = async (filters = {}) => {
	const players = await Player.find(filters);
	return players;
};

export const deletePlayer = async (id) => {
	const player = await Player.findByIdAndRemove(id);
	if (!player) {
		throw new Error('Player not found');
	}
	return player;
};
