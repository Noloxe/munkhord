import Building from '../models/building';

export const createBuilding = async (data) => {
	const building = await Building.create(data);
	return building;
};

export const updateBuilding = async (id, data) => {
	const buildingToUpdate = await Building.findById(id);
	if (!buildingToUpdate) {
		throw new Error('Building not found');
	}
	buildingToUpdate.set(data);
	return buildingToUpdate.save();
};

export const getBuilding = async (id) => {
	const building = await Building.findById(id);
	if (!building) {
		return null;
	}
	return building;
};

export const getBuildings = async (filters = {}) => {
	const buildings = await Building.find(filters);
	return buildings;
};

export const deleteBuilding = async (id) => {
	const building = await Building.findByIdAndRemove(id);
	if (!building) {
		throw new Error('Building not found');
	}
	return building;
};
