import { RESOURCES_LIST } from '../../utils/constants/resources';
import Community from '../models/community';

export const createCommunity = async (data) => {
	try {
		const community = new Community(data);
		// TODO set community in client collection
		return community.save();
	} catch (e) {
		throw new Error(e);
	}
};

export const updateCommunity = async (id, data) => {
	const communityToUpdate = await Community.findById(id);
	if (!communityToUpdate) {
		throw new Error('Community not found');
	}
	communityToUpdate.set(data);
	// TODO update community in client collection
	return communityToUpdate.save();
};

export const getCommunity = async (id) => {
	// const Community = client.Communitys.get(id) || await Community.findById(id);
	const community = await Community.findById(id);
	if (!community) {
		return null;
	}
	return community;
};

export const findCommunity = async (filters) => {
	const community = await Community.findOne(filters);
	if (!community) {
		return null;
	}
	return community;
};

export const getCommunities = async (filters = {}) => {
	const communities = await Community.find(filters);
	return communities;
};

export const deleteCommunity = async (id) => {
	const community = await Community.findByIdAndRemove(id);
	if (!community) {
		throw new Error('Community not found');
	}
	// TODO delete community in client collection
	return Community;
};

export const addCommunityBuilding = async (id, building, community = null) => {
	let c;
	if (community) {
		c = community;
	} else {
		c = await getCommunity(id);
	}
	c.resources = c.resources.map((r) => {
		const removedResource = building.constructionResources.find((item) => item.resourceId === r._id);
		if (removedResource) {
			const newResourceQuantity = r.quantity - removedResource.requirement;
			if (newResourceQuantity < 0) {
				throw new Error('Not enough resources');
			}
			return {
				...r.toObject(),
				quantity: newResourceQuantity,
			};
		}
		return r.toObject();
	});
	c.buildings.push({
		...building,
		constructionResources: building.constructionResources.map((item) => {
			if (item.resourceId === RESOURCES_LIST.TIME.id) {
				return item;
			}
			return {
				...item,
				quantity: item.requirement,
			};
		}),
	});
	return c.save();
};
