import Resource from '../models/resource';

export const createResource = async (data) => {
	const resource = await Resource.create(data);
	return resource;
};

export const updateResource = async (id, data) => {
	const resourceToUpdate = await Resource.findById(id);
	if (!resourceToUpdate) {
		throw new Error('Resource not found');
	}
	resourceToUpdate.set(data);
	return resourceToUpdate.save();
};

export const getResource = async (id) => {
	const resource = await Resource.findById(id);
	if (!resource) {
		return null;
	}
	return resource;
};

export const getResources = async (filters = {}) => {
	const resources = await Resource.find(filters);
	return resources;
};

export const deleteResource = async (id) => {
	const resource = await Resource.findByIdAndRemove(id);
	if (!resource) {
		throw new Error('Resource not found');
	}
	return resource;
};
