export const COMMUNITY = {
	MODEL_NAME: 'COMMUNITY',
	COLLECTION_NAME: 'communities',
	STATUS: {
		DRAFTED: 'DRAFTED',
		RUNNING: 'RUNNING',
		CLOSED: 'CLOSED',
	},
};

export const PLAYER = {
	MODEL_NAME: 'PLAYER',
	COLLECTION_NAME: 'players',
};

export const BUILDING = {
	MODEL_NAME: 'BUILDING',
	COLLECTION_NAME: 'buildings',
};

export const RESOURCE = {
	MODEL_NAME: 'RESOURCE',
	COLLECTION_NAME: 'resources',
};

export const BUILDINGS_ID = {
	WALL: 'WALL',
	TANKER: 'TANKER',
	FIELD: 'FIELD',
	WORKSHOP: 'WORKSHOP',
	OFFICE: 'OFFICE',
};
