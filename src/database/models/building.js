import mongoose from 'mongoose';
import { BUILDING } from './constants';

const { Schema } = mongoose;
const BuildingSchema = new Schema({
	_id: String,
	name: String,
	production: { resourceId: String, quantity: Number },
	bonus: { resourceId: String, quantity: Number },
	prerequisite: String,
	destructible: Boolean,
	constructionResources: [{ resourceId: String, quantity: { type: Number, default: 0 }, requirement: Number }],
});

export default mongoose.model(BUILDING.MODEL_NAME, BuildingSchema, BUILDING.COLLECTION_NAME);
