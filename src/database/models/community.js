import mongoose from 'mongoose';
import { COMMUNITY } from './constants';

const COMMUNITY_STATUS_ENUM = [
	COMMUNITY.STATUS.DRAFTED,
	COMMUNITY.STATUS.RUNNING,
	COMMUNITY.STATUS.CLOSED,
];

const CommunitySchema = new mongoose.Schema({
	_id: Number,
	name: { type: String, index: { unique: true } },
	players: [{
		_id: String,
		username: String,
	}],
	status: {
		type: String,
		enum: COMMUNITY_STATUS_ENUM,
		default: COMMUNITY.STATUS.DRAFTED,
	},
	resources: [{
		_id: String,
		name: String,
		quantity: Number,
		maxQuantity: Number,
	}],
	buildings: [{
		_id: String,
		name: String,
		production: { resourceId: String, quantity: Number },
		bonus: { resourceId: String, quantity: Number },
		prerequisite: String,
		active: { type: Boolean, default: false },
		destructible: Boolean,
		destroy: { type: Boolean, default: false },
		constructionResources: [{ resourceId: String, quantity: { type: Number, default: 0 }, requirement: Number }],
		level: {
			actual: { type: Number, default: 1 },
			max: Number,
		},
	}],
	categoryId: Number,
});

export default mongoose.model(COMMUNITY.MODEL_NAME, CommunitySchema, COMMUNITY.COLLECTION_NAME);
