import mongoose from 'mongoose';
import { PLAYER } from './constants';

const PlayerSchema = new mongoose.Schema({
	_id: String,
	username: String,
	resources: [{
		_id: String,
		name: String,
		emoji: String,
		quantity: Number,
		maxQuantity: Number,
	}],
});

export default mongoose.model(PLAYER.MODEL_NAME, PlayerSchema, PLAYER.COLLECTION_NAME);
