import mongoose from 'mongoose';
import { RESOURCE } from './constants';

const { Schema } = mongoose;
const ResourceSchema = new Schema({
	_id: String,
	name: String,
	emoji: String,
});

export default mongoose.model(RESOURCE.MODEL_NAME, ResourceSchema, RESOURCE.COLLECTION_NAME);
